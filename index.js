let http = require('http');

const server = http.createServer((request, response)=>{
	if (request.url == '/' && request.method == "GET"){
		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end('Welcome to Booking System')
	}
	else if (request.url == '/profile' && request.method == "GET"){
		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end("Welcome to your profile!");

	}
	else if (request.url == '/courses' && request.method == "GET"){
		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end("Here's our courses available");

	}
	else if (request.url == '/addcourses' && request.method == 'POST'){
		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end("Add courses to our resources");

	}


}).listen(4000);
console.log('Server running at localhost: 4000');